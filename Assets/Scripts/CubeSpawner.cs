using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    private Camera _cam;

    private void Awake()
    {
        _cam = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
            g.transform.position =
                _cam.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 10);
        }
    }
}
